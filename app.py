import requests
from flask import Flask, request

app = Flask(__name__)
url = 'http://13.209.15.210:8061/'


class AppRun:
    def say(self):
        return 'Hello from gitlab'

    def add(self, a, b):
        url_add = url + 'add?a=' + str(a) + '&b=' + str(b)
        with requests.Session() as session:
            with session.get(url_add) as response:
                return int(response.text)

    def sub(self, a, b):
        url_sub = url + 'sub?a=' + str(a) + '&b=' + str(b)
        with requests.Session() as session:
            with session.get(url_sub) as response:
                return int(response.text)


@app.route('/')
def say():
    return 'Hello from khpark'


@app.route('/add')
def add():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    return str(a + b)


@app.route('/sub')
def sub():
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    return str(a - b)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8061)
