import unittest
import app
import random


class TestApp(unittest.TestCase):
    def test_say(self):
        ar.say(self)

    def test_add(self):
        a = int(random.randint(0, 100))
        b = int(random.randint(0, 100))
        result = ar.add(self, a, b)
        print('add_func : ' + str(a), " + ", str(b), "\t= ", result)
        self.assertEqual(int(result), a + b)

    def test_sub(self):
        a = int(random.randint(0, 100))
        b = int(random.randint(0, 100))
        result = ar.sub(self, a, b)
        print('sub_func : ' + str(a), " - ", str(b), "\t= ", result)
        self.assertEqual(int(result), a - b)


if __name__ == "__main__":
    ar = app.AppRun
    unittest.main()
